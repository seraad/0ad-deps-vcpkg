# Build 0ad deps using vcpkg

## Usage

    vcpkg install --triplet <triplet> [ --x-install-root <directory> ]

The vars in the triplet might need adjusting to what is desired.

By default packages get installed into ./vcpkg_installed

## Triplets

https://learn.microsoft.com/en-us/vcpkg/users/triplets

### Windows

-   x64-windows-wfg
-   x86-windows-wfg

### Apple

-   x64-osx-wfg
-   arm64-osx-wfg

## Missing packages

-   gloox
-   spidermonkey
